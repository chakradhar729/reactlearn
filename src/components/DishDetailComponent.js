import React from 'react';
import { Card, CardImg, CardTitle, CardText } from 'reactstrap';


function RenderComments({ comments }) {
    if (comments == null) {
        return (<div></div>)
    }
    const comm = comments.map(comment => {
        return (
            <li key={comment.id}>
                <p>{comment.comment}</p>
                <p>--{comment.author},
                &nbps;
                {
                        new Intl.DateTimeFormat('en-US', {
                            year: 'numeric',
                            month: 'long',
                            date: '2-digit'
                        }).format(new Date(comment.date))
                    };
                 </p></li>
        )
    })
    return (
        <div className="col-12 col-md-5 m-1">
            <h4> Comments </h4>
            <ul className="list-unstyled">
                {comm}
            </ul>
        </div>
    )

}
function RenderDish({ dish }) {
    if (dish != null) {
        return (
            <div className="col-12 col-md-5 m-1 container">
                <Card>
                    <CardImg width="100%" src={dish.image} alt={dish.name}></CardImg>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </Card>
            </div>
        )
    }
    else {
        return (<div></div>)
    }
}
function DishDetail(props) {
    const dish = props.dish;
    if (dish == null) {
        return (<div></div>)
    }
    else {
        return (
            <div className="container">
                <div className="row ">
                    <RenderDish dish={props.dish}/>
                    <RenderComments comments = {props.dish.comments}/>
                </div>
            </div>
        )
    }
}



export default DishDetail;